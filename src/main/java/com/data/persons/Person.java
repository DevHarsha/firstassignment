package com.data.persons;

import com.github.javafaker.Faker;

public class Person {

    //Make these members private.
    private String name;
    private int age;
    private String company;
    private int building_code;
    private String phone_number;
    private String address;
    private Faker fake;

    Person(){

        fake = new Faker();
        this.name = fake.name().fullName();
        this.age = (int) Math.random()*90+10;
        this.company = fake.company().name();
        this.building_code = fake.code().hashCode();
        this.phone_number = fake.phoneNumber().phoneNumber();
        this.address = fake.address().fullAddress();

    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getCompany() {
        return company;
    }

    public int getBuilding_code() {
        return building_code;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public String getAddress() {
        return address;
    }
}

