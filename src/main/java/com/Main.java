package com;

import com.data.hbase.HbaseWriter;
import com.data.persons.PersonsDataGenerator;
import com.service.VariableGetter;

import java.io.IOException;

public class Main {

    public static void main(String args[]) throws IOException {

        VariableGetter variables = new VariableGetter();

        PersonsDataGenerator persons = new PersonsDataGenerator();
        persons.generate(100,System.getProperty("user.dir")+"/output");
        persons.copyFilesFromLocal(variables.HADOOP_HOME,variables.localPath,variables.hdfsPath);

        HbaseWriter hbaseWriter = new HbaseWriter();
        hbaseWriter.write(1);


    }

}
