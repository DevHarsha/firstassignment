package com.service;

import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.util.Bytes;

public class VariableGetter {

    //store constants as public, static, final. getters are not required.
    public static final String HADOOP_HOME = "/opt/homebrew/Cellar/hadoop/3.3.0/libexec/etc/hadoop/";
    public static final String localPath = "/Users/devharsha/IdeaProjects/FirstAssignment/output";
    public static final String hdfsPath = "/user/DevHarsha/";

    public static final TableName TABLE_NAME = TableName.valueOf("person");
    public static final byte[] CF_NAME = Bytes.toBytes("information");



    public static final byte[] PERSON_NAME = Bytes.toBytes("name");
    public static final byte[] PERSON_AGE = Bytes.toBytes("age");
    public static final byte[] PERSON_COMPANY = Bytes.toBytes("company");
    public static final byte[] PERSON_BUILDING_CODE = Bytes.toBytes("building_code");
    public static final byte[] PERSON_PHONE_NUMBER = Bytes.toBytes("phone_number");
    public static final byte[] PERSON_ADDRESS = Bytes.toBytes("address");

}
